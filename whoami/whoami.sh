#!/bin/bash

DIR=$(dirname $0)

PYTHON_SCRIPT="${DIR}/whoami.py"
TEMPLATE_FILE="${DIR}/whoami.html"

echo "Staring whoami on port ${WHOAMI_PORT}..."

python -u "${PYTHON_SCRIPT}" ${WHOAMI_PORT} "${CUSTOM_MESSAGE}" "${TEMPLATE_FILE}"

exit 0

