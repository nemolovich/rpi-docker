# Performance statistics

Compute some statistics about docker performances from file

## Usage

Usage:
```sh
docker build -t capgemininantes/rpi-stats ./
docker run --name stats -d -e STATS_PORT=8090 -v $(pwd)/stats-volume:/root/input:Z -p 80:8090 capgemininantes/rpi-stats
```

Acceed by `http://<IP>:<PORT>?file=<STATS_FILE>`

## File content

The file must be on the volume

The statistics file is formed like this:

```
<SERVICE_NAME>|<NUMBER_OF_REPLICAS>|<RESULT_MESSAGE>|<DURATION_TIME>
```

