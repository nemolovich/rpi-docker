#!/bin/bash

DIR=$(dirname $0)

PYTHON_SCRIPT="${DIR}/stats.py"
TEMPLATE="${DIR}/stats.html"

echo "Staring stats on port ${STATS_PORT}..."

python -u "${PYTHON_SCRIPT}" "${TEMPLATE}" ${STATS_PORT} "${FILES_VOLUME}"

exit 0

